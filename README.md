## **Proyecto:** Mapa georreferencial de OpenStreetMap en sitio web



### **Autor:** Emanuel Castillo



#### **Resumen**
 
El proyecto consiste en la implementación de un mapa georreferencial de [OpenStreetMap](https://www.openstreetmap.org/#map=4/-40.44/-63.59), en un servidor local y en un servidor web o nube, con marcadores en localidades determinadas, en este caso se usarán de ejemplo las localidades de la Provincia de Santa Fe adheridas a el Programa Munigestión. Este trabajo es el de dar a conocer las alternativas que presenta el mundo del software libre para poder desplegar un mapa de codigo abierto como es OpenStreeMap, demostrando que el software libre es tan bueno o mejor que el privativo. OpenStreetMap se trata de una plataforma libre y colaborativa, donde el trabajo conjunto de los editores va diseñando los mapas, introduciendo los cambios, o simplemente completando zonas vacías, etc.  Es por eso que se alimenta de la participación voluntaria y del esfuerzo de todo tipo de personas anónimas.
El proyecto se compartirá a la comunidad para que se pueda aportar y mejorar. Ademas se explicará la forma de poder replicarlo. 

El Programa Munigestion surge en 2014 a partir del Convenio entre UNL y UNR, generando lineamientos de acción y ejecución para brindar capacitación a los gobiernos 
locales de la Provincia de Santa Fe y a sus agentes, entendiéndola como una herramienta prioritaria para el fortalecimiento de las capacidades de gestión 
e innovación política. Por lo tanto, el mayor énfasis puesto desde este Programa de Extensión Universitaria es ofrecer propuestas de capacitación a los 
gobiernos locales propiciando articulaciones entre la misión universitaria de la extensión y las políticas públicas. Actualmente el Programa depende de la 
Secretaria de Fortalecimiento Territorial de la Universidad Nacional del Litoral.

En este mapa georreferencial estaran marcadas todas las localidades de la Provincia de Santa Fe que tienen convenio firmado en el marco del Programa, 
junto a datos de las mismas y sus participaciones en las distintas modalidades del Munigestión.

En principio para desplegar el mapa iba a utilizar [Umap](http://umap.openstreetmap.fr/es/). Es un servicio web de código abierto (licencia de software WTFPL) que permite crear mapas personalizados con capas de OpenStreetMap para embeberlos en un sitio web. ¿Pero que sentido tiene crear un mapa personalizado desde una aplicacion?  Es por eso que en este proyecto vamos a crear un mapa web  de [Leaflet](https://leafletjs.com/) utilizando los principales lenguajes de programación:

* HTML es el lenguaje de marcado de páginas Web y define la estructura del contenido de una página Web.
* ¿Qué es lo que hace bonita a una página web? Eso es CSS (Cascading Style Sheets u hojas de estilo en cascada).
* JavaScript es el lenguaje de programación Web para crear aplicaciones que se ejecutan en el navegador y le da funcionalidad a las aplicaciones.

La idea era tambien poder desplegarlo sin conexion pero debido a que se debian usar otras apliaciones o descargar teselas (imágenes del mapa) una por una descarté esta posibilidad.

#### **Estado del arte** 

Indagando en la web encontré un proyecto similar llamado "[MapServer](https://mapserver.org/)" pero mucho mas complejo, ya que fue originalmente desarrollado por la Universidad de 
Minnesota (UMN) con el proyecto ForNet en cooperación con la NASA, y el departamento de recursos naturales (MNDNR). Más tarde fue recibido por el proyecto 
TerraSIP, un proyecto patrocinado entre la NASA y la UMN y un consorcio de intereses en el manejo de la tierra.
MapServer es un motor de representación de datos geográficos de codigo abierto escrito en C. Además de navegar por los datos SIG, MapServer permite
crear "mapas de imágenes geográficas", es decir, mapas que pueden dirigir a los usuarios al contenido. Por ejemplo, Minnesota DNR RecreationCompass 
proporciona a los usuarios más de 10,000 páginas web, informes y mapas a través de una sola aplicación. La misma aplicación sirve como un "motor de mapa" 
para otras partes del sitio, proporcionando un contexto espacial donde sea necesario. 

[MapServer](https://mapserver.org/) es uno de los proyectos fundadores de la fundación OSGeo , y es mantenido por un número creciente de desarrolladores (cerca de 20) de todo 
el mundo. Es apoyado por un grupo diverso de organizaciones que financia mejoras y mantenimiento, y administrado dentro de OSGeo por el Comité Directivo
del Proyecto MapServer compuesto por desarrolladores y otros colaboradores. Todo el código fuente está disponible de forma abierta a través de GitHub .

Se diferencia mucho en el trabajo que voy a hacer, por ser mas complejo como mencioné antes y porque es destinado a una cantidad mayor de usuarios. 
Mi trabajo va a ser destinado especialmente a los que participan en el Programa Munigestion, ya que en el mapa solo van a estar marcadas las localidades 
vinculadas al programa.

Parte de mi proyecto se va a basar en el proceso de desarrollo de [MapServer](https://mapserver.org/) solo para usarlo de referencia ya que contiene mucho mas herramientas  y 
paquetes. 


#### **Objetivos** 

**General:**

Este trabajo tiene como objetivo general mostrar la forma de replicar el mapa para que se utilice con motivos propios y compartirlo a la comunidad para que se pueda mejorar. Ademas de dar a conocer la alternativa que propone el Software Libre con OpenStreetMap al desplegar un mapa georreferencial en nuestra pagina web demostrando que el software libre es tan bueno o mejor que el privativo. En este caso a modo de ejemplo se mostrará la influencia del programa Munigestion en toda la Provincia de Santa Fe, mediante un mapa georreferencial alojado en un servidor local y en un servidor web o nube  que contenga todas las localidades adheridas marcadas. Con esto se pretende mejorar y facilitar el acceso a la informacion del programa, mostrando todas las localidades adheridas con sus datos de participaciones en las distintas modalidades del programa y sus estados de situacion, a fin de impactar a los gobiernos locales con el alcance del mismo. Ademas de promover y difundir para trasmitir interes a las localidades no adheridas. 

**Especificos:**
 
* Mostrar la alternativa que ofrece el Software Libre para desplegar un mapa en un sitio web
* Mostrar la forma de replicar el mapa para uso propio
* Compartir a la comunidad para que se pueda aportar y mejorar el proyecto
* Mostrar el alcance del programa en la Provincia de Santa Fe
* Informar a los agentes de los gobiernos locales y a las autoridades de las localidades adheridas de su estado de situacion en el programa
* Mostrar a los agentes de los gobiernos locales y a las autoridades de las localidades adheridas sus participaciones en las distintas modalidades del programa
* Promover y transmitir interes a las localidades no adheridas

#### **Metodologia:**

En principio procedo a instalar el [Leaflet](https://leafletjs.com/) en la maquina y descargar los archivos necesarios para desplegar el codigo. [Leaflet](https://leafletjs.com/) es una librería open-source JavaScript que permite crear mapas interactivos para la web y para entorno móvil de manera fácil. Con esta biblioteca se puede usar mapas base de Google, ESRI, OpenStreetMap, entre otros, agregar marcadores, polígonos, líneas, realizar zoom y extender su funcionalidad con una buena cantidad de plugins, entre ellos, muchos desarrollados por ESRI.
Una vez instalado procedo a desarrollar el codigo html, la hoja de estilo .css y los archivos .js (Javascript) con Notepad++. Los archivos seran los siguientes:
* index.html : Aqui se desplegará el codigo para poder visualizar el mapa con Leaflet donde tambien irá añadido el archivo localidades.js y la hoja de estilo map.css
* map.css : Hoja de estilo 
* map.js : Aqui se desplegará la capa base y la funcion para los popups o marcadores. Tambien irá añadida la capa con L.geoJson y la variable del objeto geoJSON añadida en el archivo js
* localidades.js : Este va a ser el archivo GeoJSON con la informacion de los popups o marcadores 

En el archivo localidades.js van a estar los marcadores de las localidades de la Provincia de Santa Fe vinculadas al Programa Munigestion. Estos marcadores o popups van con una descripcion a modo de etiqueta o ventana con los datos de cada una, tales como el nombre de la localidad, cant. de habitantes, tipo de gobierno local, autoridad actual, partido politico de la autoridad, que gobierno local tiene convenio firmado con el programa, coordenadas, entre otros. Para hacer esto voy a utilizar GeoJSON. GeoJSON es un formato estándar abierto diseñado para representar elementos geográficos sencillos, junto con sus atributos no espaciales, basado en JavaScript Object Notation. El formato es ampliamente utilizado en aplicaciones de cartografía en entornos web al permitir el intercambio de datos de manera rápida, ligera y sencilla. La gramática del formato está basada en el estándar WKT del Open Geospatial Consortium, con unas geometrías que pueden ser de tipo punto (direcciones, ubicaciones, puntos de interés, etc.), líneas (calles, carreteras, fronteras, etc.), polígonos (países, provincias, parcelas catastrales, etc.) y colecciones de estos tipos. GeoJSON usa un sistema de referencia de coordenadas geográficas, WGS84 y unidades en grados decimales.

En resumen, el codigo html va a contener la libreria Leaflet para poder visualizar el mapa de [OpenStreetMaps](https://www.openstreetmap.org/), va a estar vinculada la hoja de estilo  y los archivos .js (Javascript) necesarios. Tambien habrá una lista de las localidades.
El desarrollo de la pagina web toma un tiempo aproximado de 7 dias. 

Cuando ya tenga el mapa ,desde la Universidad procedo a alojarlo en un servidor local usando [XAMPP](https://www.apachefriends.org/es/index.html). Asi se podrá compartir a cualquier persona que esté conectada a la misma red. Se podrá usar en reuniones por ejemplo. Tambien lo alojaria en GitLab, ya que conviene mas que alojarlo en un servidor web. GitLab tiene una herramienta llamada [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), que te permite publicar el código del sitio en vivo en la Web. Con esto puedo usar el control de versiones Git para ir modificando el mapa (agregar nuevas localidades, actualizar datos, etc). Esto es para compartirlo publicamente a cualquier persona de la Provincia que este interesada en ver el alcance del programa. Otra forma de compartirlo en la web es embeberlo en la pagina oficial del Programa. 
El tiempo aproximado de este ultimo paso es de 2 dias.


#### **Metodologia por pasos para replicar proyecto:**

A continuacion explicaré paso por paso como crear un mapa de [OpenStreetMap](https://www.openstreetmap.org/) junto a una lista de los marcadores. Este mapa va a contener marcadores con las localidades vinculadas al Programa Munigestion de la UNL a modo de ejemplo para poder replicarlo y ponerle los marcadores que se necesiten. Luego lo alojaremos en un servidor local usando [XAMPP]([https://www.apachefriends.org/es/index.html]) y en un servidor web usando [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). [XAMPP](https://www.apachefriends.org/es/index.html) es un paquete de software libre, que consiste principalmente en el sistema de gestión de bases de datos MySQL, el servidor web Apache y los intérpretes para lenguajes de script PHP y Perl. GitLab Pages te da la oportunidad de crear páginas web utilizando varios generadores de sitios estáticos. Algunos de los más conocidos son Jekyll, Pelican, Hugo, etc. 

El procedimiento está basado en un ejemplo de la [Leaflet School](https://github.com/sigdeletras/leaflet-school), [Leaflet con Capa GeoJSON desde archivo js](http://bl.ocks.org/sigdeletras/931e41a6cb34936a2079e97a3b7ccca6).

Requerimientos:

* Un editor de texto avanzado para programadores, multiplataforma. 
* [XAMPP](https://www.apachefriends.org/es/index.html)

Para incrustar nuestro mapa en un sitio web, de mismo modo en que se puede con los mapas de Google o Yahoo, se puede usar alguna de las siguientes bibliotecas de mapas de JavaScript:

* Leaflet ([leafletjs.com](http://leafletjs.com)) es una biblioteca JavaScript libre de código abierto con una API ligera y fácil de usar, con muchos complementos
* OpenLayers ([openlayers.org](http://openlayers.org)) es biblioteca JavaScript enriquecida libre de código abierto.

En este caso se desplegará el mapa con Leaflet, ya que me pareció mas conveniente y mejor que OpenLayers por varios motivos:
*  Más sencillo y rápido de aprender (con un estilo de código moderno)
*  Mejor arquitectura y diseño interno que OpenLayers, por haber aparecido después, en el año 2011
*  Su look and feel (botones y layer swicther, por ejemplo), aunque en este apartado están muy parecidos porque OpenLayers tiene un aspecto moderno, Leaflet quizá lo es un poco más. Esto es muy subjetivo
*  Soporte móvil
*  Ecosistema de plugins para extender la funcionalidad de la la librería. Aunque OpenLayers también tiene librerías de terceros, apenas llega a la docena de plugins, mientras que para Leaflet hay unos 200 plugins
*  La documentación de la API de Leaflet es más sencilla de entender para usuarios nóveles, la documentación de la API de OpenLayers se hace más complicada de comprender

Entre que es más sencillo aprender, la documentación es más clara y a través de los plugins es posible añadir prácticamente cualquier funcionalidad (calcular rutas óptimas, incluir una mapa de localización, añadir capas base de cualquier proveedor, incluir buscadores, ….) el ganador es Leaflet. Ojo, siempre y cuando el propósito del proyecto que queramos desarrollar sea general, puesto que para cosas concretas hay que analizar bien las funcionalidades de cada librería.

Para los marcadores usaremos un formatos de datos vectorial. Una de las posibilidades que evalué y descarté fue la de [Shapefile](https://mappinggis.com/tag/shapefile/). Debido a su simplicidad, los shapefiles han sido la forma estándar de almacenar e interactuar con los datos espaciales desde que el software GIS apareció por primera vez.
El shapefile es el formato de datos vectorial más popular y extendido entre la comunidad GIS. Sin embargo, estos archivos tienen una serie de desventajas:

1.  Un shapefile no es un único archivo, se compone de varios archivos que un cliente SIG lee como uno único. El mínimo requerido es de tres:
* .shp – almacena las entidades geométricas de los objetos.
* .shx – almacena el índice de las entidades geométricas y el
* .dbf – tabla dBASE donde se almacenan los atributos de los elementos geométricos.

2.  Está diseñado para almacenar datos sencillos. El tamaño máximo permitido del archivo está restringido a 2GB (dbf).
3.  Carece de capacidad para almacenar información topológica.
4.  Un shapefile no permite nombres de campo con más de 10 caracteres y con restricciones.
5.  Solamente puede almacenar una geometría por tabla.
6.  No pueden almacenar valores nulos, redondean números, tienen poca compatibilidad con las cadenas de caracteres Unicode. No pueden almacenar fecha y hora en un campo.
7.  Usuarios concurrentes pueden causar la corrupción de los shapefiles. Si bien es posible escribir código adicional para asegurarse de que varias escrituras en el mismo archivo no dañen los datos, cuando se haya resuelto este problema y también el del rendimiento asociado, habrás escrito una buena parte de una base de datos.

Una vez conocidas todas las desventajas que tiene este sistema de almacenamiento de información espacial, podemos optar por utilizar una sistema gestor de bases de datos espaciales, ¿por qué?

1.  Los archivos requieren un software especial para su lectura y escritura. SQL es una abstracción para el acceso a los datos y el análisis. Sin esa abstracción, necesitaríamos escribir todo el código de acceso y análisis nosotros.
2.  Operaciones y cálculos complejos requieren un software complejo para las respuestas. Operaciones complejas e interesantes como los joins espaciales, agregaciones, etc, se pueden expresar en una sola línea de SQL, pero ocupa cientos de líneas de código especializado a la hora de programar contra shapefiles. 
3.  Una base de datos espacial soporta tipos de datos espaciales, índices espaciales y tiene cientos de funciones espaciales.

[GeoJSON](http://geojson.org/) (Javascript Object Notation) es un sustituto perfectamente adecuado del Shapefile en muchos escenarios (visualización e intercambio de datos). Es un formato de texto que es muy rápido de analizar en máquinas virtuales Javascript. Ademas es un formato de intercambio de datos geoespaciales basado en JSON. Este formato apareció en 2008 y puede representar una geometría, un fenómeno o una colección de fenómenos. Es un formato muy popular en las aplicaciones web mapping que hace que nos ahorremos la parte de la base de datos y el servidor.
GeoJSON, a diferencia de los shapefile, se basan en estándares de la OGC, se han diseñado para almacenar datos complejos y voluminosos. Además los atributos de las geometrías pueden contener nombre muy largos. Es por eso que el elegido  para crear los marcadores fue GeoJSON.


1- En primer lugar se procede a elaborar el marco HTML básico en su editor favorito. La estructura que tendrá nuestro HTML es la siguiente:

```
<html>
<head>
	
</head>
<body>

</body>
</html>
```

Guardar con el nombre index.html

2 - Dentro de head incluiremos la codificación de caracteres, la etiqueta meta viewport para controlar la composición en los navegadores móviles y el titulo de nuestra pagina web:

```
<head>
	<meta charset=utf-8 />
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-			scale=1.0, user-scalable=no, minimal-ui' />
	<title>Localidades vinculadas al Programa Munigestion</title>
```


3 - Para crear un mapa con Leaflet, siguiendo en la cabecera head incluiremos la libreria JavaScript leaflet.js (que contiene el código de la libreria) y la hoja de estilo leaflet.css (con la hoja de estilos de la librería):

```
<head>
	<script src="https://unpkg.com/leaflet@1.3.3/dist/leaflet.js"></script>

	<link href="https://unpkg.com/leaflet@1.3.3/dist/leaflet.css" rel='stylesheet' /> 
```

El código que se muestra, carga los archivos de estilo CSS y JavaScript desde el CDN de Leaflet.

Otra alternativa es descargar esos archivos, colocarlos en las carpetas js y css cada uno donde corresponda e incluirlos en el codigo:
```
<head>
	<script src="js/leaflet.js"></script>

	<link href="hcss/leaflet.css" rel='stylesheet' /> 
```

4 - Crear una hoja de estilo .css asegurando que el contenedor de mapa tenga un tamaño definido y que se posicione a la derecha de la pagina. Agregando un color de fondo y un borde para el mapa:

```
body {
        margin: 0;
        padding: 0;
        background: #AED6F1;
    }
    #map {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 375px;
        width: 80%;
        border: black 5px solid;
    }
```


Guardar con el nombre map.css y dentro de una carpeta nueva con el nombre "css". La carpeta debe crearse en la misma ruta que el index.html
Dentro de la etiqueta head de nuestro html incluiremos este archivo con:

`<link rel="stylesheet" href="css/map.css">`

5 - Dentro de la etiqueta body de nuestro html escribiremos el marcado para el mapa, que genera un único elemento de documento. También damos al contenedor un atributo id para que podamos hacer referencia a él en nuestro código:

```
<body>
<div id="map"></div>
```

6 - Lo que haremos ahora inicializar el mapa en el div "map" con un centro dado y zoom. Estas coordenadas y nivel de zoom son los que definen donde se posiciona el mapa una vez que se abre. Para ello crearemos un archivo con el siguiente codigo:

`var map = L.map('map').setView([-31.671,-58.770], 7);`

Guardar con el nombre map.js dentro de una carpeta nueva con el nombre "js". La carpeta debe crearse en la misma ruta que el index.html 
Dentro de la etiqueta body de nuestro html incluiremos este archivo con:

`<script src="js/map.js"></script>`

L.map es la clase central de la API. Se usa para crear y manipular el mapa. En el mapa establecemos unas coordeanas de la vista y un nivel de zoom. En este caso al abrir el mapa se ubicaria automaticamente en la Provincia de Santa Fe.

7 - A continuación en el mismo archivo añadimos un mapa base como tile layer, en este caso las imágenes de OSM. Crear un tile layer lleva implícito establecer la URL y el texto con la atribución:

```
var osmLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap<\/a> contributors'
}).addTo(map);
```


8 - En el mismo archivo vamos a añadir una funcionalidad: al hacer click sobre el mapa se abrirá una ventana emergente con el nombre de la localidad y una descripcion que vamos a hacer mas adelante.

El siguiente código usa la funcionalidad popup de Leaflet:

```
function popUpInfo(feature, layer) {
    
    if (feature.properties && feature.properties.Name) {
        layer.bindPopup("<b>"+feature.properties.Name	+"</b><br>"+feature.properties.description+"");
    }
}
```


Esta función comprueba que la parte propierties del GeoJSON existe, así como la propiedad del campo NAME, que es único para el archivo de datos. La función layer.bindPopup construye la funcionalidad de clic en el mapa. GeoJSON es un formato de intercambio de datos para una variedad de estructuras de datos geográficos. GeoJSON se puede utilizar para representar una geometría, una entidad, una colección de geometrías o una colección de entidades.

9 - Para ya cerrar este archivo añadimos la función que crea las ventanas emergentes. onEachFeature es una propiedad de L.geoJson que se puede pasar con una función:

```
L.geoJson(localidades, {
    onEachFeature: popUpInfo
    }).addTo(map);
```


10 - A continuacion lo que se debe hacer es crear archivo Javascript con la capa GEOJson donde iran los marcadores (coordenadas y descripcion). En [este](http://wiki.geojson.org/GeoJSON_draft_version_6) link se explica detalladamente como realizar un FeatureCollection con GEOJson, ademas de otros tipos de geometria. Un objeto GeoJSON con el tipo "FeatureCollection" representa una colección de objetos de características.

Crear un archivo Javascript con el siguiente codigo: 

```
var localidades = {
"type": "FeatureCollection",
"name": "Localidades",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "San Justo", "description": "ESCRIBIR LA DESCRIPCION DEL MARCADOR AQUI" }, "geometry": { "type": "Point", "coordinates": [ ESCRIBIR COORDENADAS DEL MARCADOR AQUI ] } },
```


Guardaremos el archivo con el nombre localidades.js dentro de la carpeta js. Luego lo incluiremos en nuestro html dentro de la etiqueta <head> con lo siguiente: 

`<script type="text/javascript" src="js/localidades.js"></script>`

En este archivo se define la variable localidades para los marcadores popups, junto al nombre de la localidad y una descripcion. En cada marcador se coloca las coordenadas para ubicarlo dentro del mapa. 

FeatureCollection no es mucho más que un objeto que tiene "type": "FeatureCollection" y luego una matriz de objetos Feature bajo la tecla "features" . Como su nombre indica, la matriz debe contener solo objetos de característica, no geometrías en bruto.
FeatureCollections como objetos tiene mucho sentido en términos de los aspectos comunes entre los diferentes tipos de GeoJSON.
Los objetos GeoJSON son objetos, no matrices o primitivos.
Los objetos GeoJSON tienen una propiedad "type".
Esto es realmente ingenioso para las implementaciones: no necesitan adivinar qué tipo de objeto GeoJSON están viendo, solo leen la propiedad "type".

"name" no es mas que el nombre de cada marcador.

En "crs", urn es el identificador, ogc es la organización, def es otra operación estática, crs es el tipo (sistema de referencia de coordenadas), OGC es la autoridad, 1.3 es la versión y CRS84 es la proyección. Esto es para que GeoJson pueda tomar las coordenadas de nuestro sistema de geocodificacion. 

11 - Por ultimo en nuestro archivo index.html vamos a proceder a crear una lista con todas las localidades que iran en los marcadores.
Dentro de body creamos una cabecera h1 con el titulo:

`<h1> Localidades vinculadas al Programa Munigestión: </h1>`

Luego crearemos una lista ordenada mediante numeracion y cada localidad: 
```
<ol type ="decimal-leading-zero">
	<li>Localidad 1</li>
	<li>Localidad 2</li>
</ol>
```


12 - Por ultimo en la hoja de estilos map.css agregaremos el formato del titulo y la lista:
```
h1 {
        font: oblique bold 150% cursive;
        padding-left: 15px;
        text-decoration: underline;
        
    }

    ol {
        line-height: 30px;
        font-weight: bold;
    }
```


El titulo tendra una fuente oblique en negrita y cursiva con un tamaño de 150%. Una medida de 15px desde el borde izquierdo de la pagina e irá subrayado.

La lista irá con un tamaño de 30px y una fuente en negrita.

Asi este proyecto da lugar a que se pueda replicar y mejorar.

Ya tendremos nuestro mapa de OSM desplegado en un sitio web, a continuacion lo que se hará es alojarlo en un servidor local, en este caso usando [XAMPP](https://www.apachefriends.org/es/index.html). 


1.  Lo primero que se debe hacer es descargar XAMPP desde la [pagina oficial](https://www.apachefriends.org/es/index.html), ve a la sección de Linux. Hay múltiples versiones disponibles, la diferencia es principalmente la versión de PHP. Selecciona cualquier versión de tu preferencia.
2.  Para ejecutar el instalador, primero necesitamos darle permisos de ejecución. Abre el terminal y ahí ve hasta el directorio donde descargaste el instalador y hazlo ejecutable usando el siguiente comando (en este caso el nombre del archivo es: xampp-linux-x64-7.3.2-0-installer.run cámbialo al nombre de tu archivo). 
sudo chmod +x xampp-linux-x64-7.3.2-0-installer.run 
Luego ejecuta el instalador escribiendo lo siguiente: sudo ./xampp-linux-x64-7.3.2-0-installer.run
Deberías ver la ventana de instalación, sigue los pasos con las opciones por defecto y el instalador se hará cargo del resto. Al final de la instalación, puedes escoger si iniciar o no XAMPP despúes de hacer click en el botón de finalizar.
3.  A continuacion se probaremos los componentes. La ventana principal de XAMPP cuenta con un panel de control del las aplicaciones del servidor, donde es posible iniciarlas, pararlas e inclusive configurarlas. Ve al tab de manejo de servidores (manage servers) para verificar el estado de las aplicaciones e iniciemos el servidor apache y MySql.
Abre el navegador web y ve a http://localhost/ debería cargar el website por defecto de XAMPP, lo que significa que nuestro servidor apache esta funcionando.
4.  Para que nuestros fichero html sea interpretado por el navegador web desde http://localhost, debemos guardarlos en la carpeta /opt/lampp/htdocs
Asi si guardamos todas las carpetas (js y css) y el archivo index.html con el codigo en /opt/lampp/htdocs accederemos a el escribiendo http://localhost/index.html en el navegador web.


Para alojarlo en un servidor web en principio pensé en utilizar un hosting gratuito, pero si los archivos de salida son solo HTML, CSS y JavaScript porque no usar GitLab Pages y evitar inconvenientes?
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), te da la oportunidad de crear páginas web utilizando varios generadores de sitios estáticos. Algunos de los más conocidos son Jekyll, Pelican, Hugo, etc. Veremos cómo utilizar GitLab Pages para alojar la página de un proyecto pero primero hay que conocer algunos conceptos de GitLab:

**GitLab CI / CD:** Sirve para numerosos propósitos, para construir, probar e implementar su aplicación desde GitLab a través de métodos de integración continua (CI) y entrega continua y despliegue (CD). 
Para profundizar mas este concepto consulte la documentacion de [GitLab CI / CD](https://docs.gitlab.com/ee/ci/README.html).

**GitLab Runner:** Es el proyecto de código abierto que se utiliza para ejecutar sus trabajos y enviar los resultados a GitLab. Se utiliza junto con GitLab CI , el servicio de integración continua de código abierto incluido con GitLab que coordina los trabajos.
Para profundizar mas este concepto consulte la documentacion de [GitLab Runner](https://docs.gitlab.com/runner).

1.  Lo primero abrir una cuenta en GitLab, puedes registrarte mediante tus credenciales de GitHub. Así que creamos un proyecto y subimos nuestro código html. Con su archivo index.html, la carpeta "css" con las hojas de estilo css y la carpeta "js" con los archivos js.
2.  Una vez subido el código, tienes que crear e incluir un archivo llamado .gitlab-ci.yml que deberás incluir en la raíz del proyecto, y que contendrá el siguiente código:

```
pages:
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master
```
Con este archivo lo que hace es crear la página del proyecto.

pages: Es el nombre de la etapa CI. Puede tener varias etapas, por ejemplo, 'Test', 'Build', 'Deploy', etc. 
script: comienza la siguiente parte de la etapa CI, indicándole que comience a ejecutar los siguientes scripts: mkdir .public (crea un nuevo directorio), cp -r * .public (copia su proyecto en ese directorio), mv .public public (obtiene acceso a él moviendolo a la raiz del proyecto).

artifacts: y paths: se utilizan para decirle a las páginas de GitLab dónde se guardan los archivos estáticos. only: y master le dice al CI que solo ejecute las instrucciones anteriores cuando se implementa la rama maestra.

Para implementar su sitio, GitLab usará su herramienta integrada llamada GitLab CI / CD , que construirá su sitio y lo publicará en el servidor de GitLab Pages. La secuencia de scripts que ejecuta GitLab CI / CD para llevar a cabo esta tarea se crea a partir de este archivo, que puede crear y modificar a voluntad.

Lo que realmente hace este archivo es decirle a GitLab Runner que ejecute scripts como lo haría desde la línea de comandos. El Runner actúa como tu terminal. GitLab CI / CD le dice al Runner qué comandos ejecutar. Ambos están incorporados en GitLab, y no es necesario configurar nada para que funcionen.


3.  El software comienza a realizar las tareas y una vez terminado el proceso si todo ha ido correctamente podrás encontrar tu página en la dirección: https://nombredeusuario.gitlab.io/nombredelproyecto
Después cada vez que quieras hacer cambios en tu web, deberás modificar el archivo que desees, realizar los commits pertinentes y enviar el pull para que el cambio llegue al repositorio, y el software de GitLab de nuevo compilará todo para actualizar la página web.

Este método es una manera de publicar una página web de un proyecto que quieras dar a conocer, sin necesidad de comprar un dominio, ni nada de eso. Como contrapartida no tienes manera de tener estadísticas de visitas, ni forma de cambiar el nombre del dominio… Limitaciones que tienes que tener en cuenta.


#### **Forma de aportar al proyecto:**

La forma de poder aportar a este proyecto es haciendo un fork para hacer públicos los cambios, como una manera abierta de participación.

Luego crear un Merge Request, para poder abrir una discusión para la revisión del código y que se pueda discutir acerca de los cambios.

1.  Haga clic en el botón Fork ubicado en el centro de la página o en la página de inicio de un proyecto justo al lado del botón de estrellas.
2.  Una vez que haga eso, se le presentará una pantalla donde puede elegir el espacio de nombres para bifurcar. Solo se mostrarán los espacios de nombres (grupos y su propio espacio de nombres) a los que tenga acceso de escritura. Haga clic en el espacio de nombres para crear su fork allí.
Una vez finalizada la bifurcación, puede comenzar a trabajar en el repositorio recién creado. Allí, tendrá acceso completo al Propietario ,por lo que puede configurarlo como desee.
3.  Una vez que esté listo para enviar su código al proyecto principal, debe crear un Mergeu Request (solicitud de combinación). Elija el branch principal del proyecto bifurcado como origen y el branch "develop" del proyecto original como destino y cree un Mergue Request.
4.  Luego, puede asignar el Mergue Request a alguien para que revise sus cambios, en este caso a mi. 

**Sugerencias de aportes:**
Hay muchas funcionalidades que se podrian agregar, como por ejemplo:

*  Al clickear en alguna localidad de la lista, en el mapa se muestre el marcador correspondiente. 
*  A esta funcion tambien se le puede agregar un zoom que se acerce a la localidad. 
*  Si se requiere agregar más sitios al mapa, en vez de hacerlo manualmente que se pueda cargar desde la misma interfaz web con un usuario con esos permisos. 



#### **Proyecto en GitLab Pages:** https://emanueladriancastillo.gitlab.io/Trabajo-Final-Tec.SL/

#### **Licenciamiento y Publicacion:** 

Este proyecto será publicado en Gitlab bajo GNU GENERAL PUBLIC LICENSE v3.0

#### **Documentación:**

La documentacion del proyecto se llevara acabo en este mismo proyecto de Gitlab.

https://gitlab.com/emanueladriancastillo/Trabajo-Final-Tec.SL

